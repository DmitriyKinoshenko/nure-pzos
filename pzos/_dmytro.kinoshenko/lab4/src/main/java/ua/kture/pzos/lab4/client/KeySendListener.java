package ua.kture.pzos.lab4.client;

/**
 * Created by DimaK on 15.11.2014.
 */
interface KeySendListener {
    void send();
}
