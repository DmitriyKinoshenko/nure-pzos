package ua.nure.pzos.lab2.auction;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.impl.SimpleLogger;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AuctionBehaviourTest {

    @BeforeClass
    public static void configureLogger() {
        System.setProperty(SimpleLogger.SHOW_DATE_TIME_KEY, "true");
    }

    @Test
    public void makesAuction() {
        Lot fordFocus = new Lot("Ford Focus", 2_000);
        Lot ferrari = new Lot("Ferrari", 100_000);
        Lot tiguan = new Lot("VW Tiguan", 10_000);
        Lot rav4 = new Lot("Toyota Rav4", 15_000);

        var lots = List.of(
                fordFocus,
                ferrari,
                tiguan,
                rav4
        );
        var bidders = List.of(
                new Bidder("Anna", 25_000),
                new Bidder("Sergey", 30_000)
        );

        Auction auction = new Auction(lots, bidders);

        auction.startAuction();

        auction.finishAuction();

        // sold
        assertThat(fordFocus.getWinningBidder()).isNotNull();
        assertThat(tiguan.getWinningBidder()).isNotNull();
        assertThat(rav4.getWinningBidder()).isNotNull();

        // not sold - too expensive
        assertThat(ferrari.getWinningBidder()).isNull();

        long wonLotsCount = bidders.stream().mapToLong(bidder -> bidder.getWonLots().size()).sum();
        assertThat(wonLotsCount).isEqualTo(3);
    }

}
