package ua.nure.pzos.lab2.auction;

public interface LotActivityObserver {

    void newLot(Lot lot);

    void lotPriceChanged(Lot lot);

    void lotIsSold(Lot lot, Bidder winner);
}
