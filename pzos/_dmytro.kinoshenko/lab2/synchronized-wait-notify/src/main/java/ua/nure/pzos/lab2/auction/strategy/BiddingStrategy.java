package ua.nure.pzos.lab2.auction.strategy;

import ua.nure.pzos.lab2.auction.Lot;

import java.util.Optional;

public interface BiddingStrategy {

    Optional<Double> makeBid(double balance, Lot lot);

}
