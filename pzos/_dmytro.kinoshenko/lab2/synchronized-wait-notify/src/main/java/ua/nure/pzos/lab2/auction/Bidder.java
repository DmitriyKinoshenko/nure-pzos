package ua.nure.pzos.lab2.auction;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import ua.nure.pzos.lab2.auction.exception.BidAlreadyMadeException;
import ua.nure.pzos.lab2.auction.exception.BidException;
import ua.nure.pzos.lab2.auction.exception.BidTooLowException;
import ua.nure.pzos.lab2.auction.exception.LotAlreadySoldException;
import ua.nure.pzos.lab2.auction.strategy.BiddingStrategy;
import ua.nure.pzos.lab2.auction.strategy.HonestBiddingStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Getter
@Slf4j
@ToString(exclude = {"wonLots", "biddingStrategy", "currentLot"}, includeFieldNames = false)
public class Bidder extends Thread implements LotActivityObserver {
    private static final int MIN_THINKING_TIME_MSEC = 1_000;
    public static final int MAX_THINKING_TIME_MSEC = 5_000;

    private final String bidderName;
    private final BiddingStrategy biddingStrategy;

    private final List<Lot> wonLots = new ArrayList<>();

    private double balance;

    private boolean toLeave = false;

    private Lot currentLot;

    public Bidder(String bidderName, double balance, BiddingStrategy biddingStrategy) {
        this.bidderName = bidderName;
        this.balance = balance;
        this.biddingStrategy = biddingStrategy;

        setName("Thread-" + bidderName);
    }

    public Bidder(String bidderName, double balance) {
        this(bidderName, balance, new HonestBiddingStrategy());
    }

    public void newLot(Lot lot) {
        makeBid(lot);
    }

    public void lotPriceChanged(Lot lot) {
        makeBid(lot);
    }

    public void lotIsSold(Lot lot, Bidder winner) {
        if (winner == this) {
            balance -= lot.getCurrentPrice();
            wonLots.add(lot);
        }
    }

    public void enterAuction() {
        start();
    }

    @SneakyThrows
    @Override
    public void run() {
        log.info("{} is in action", bidderName);

        while(true) {
            synchronized (this) {
                while(currentLot == null && !toLeave) {
                    wait();
                }

                if (toLeave) {
                    break;
                }

                processLot(currentLot);
                currentLot = null;
            }
        }

        log.info("{}: bye-bye!", this);
    }

    private synchronized void makeBid(Lot lot) {
        currentLot = lot;

        notify();
    }

    @SneakyThrows
    private void think() {
        int thinkTime = MIN_THINKING_TIME_MSEC +
                ThreadLocalRandom.current().nextInt(MAX_THINKING_TIME_MSEC - MIN_THINKING_TIME_MSEC);

        log.info("{}: thinking for {}", this, thinkTime);

        TimeUnit.MILLISECONDS.sleep(thinkTime);
    }

    public synchronized void leave() {
        toLeave = true;

        notify();
    }

    private void processLot(Lot lot) {
        if (lot.getWinningBidder() == this) {
            return;
        }

        think();

        Optional<Double> toMakeABid = biddingStrategy.makeBid(balance, lot);
        if (toMakeABid.isEmpty()) {
            log.info("{}: skipping a bid for {}", this, lot);
            return;
        }

        double newBidPrice = toMakeABid.get();
        try {
            log.info("{}: Making a bid for a {} with {} amount", this, lot, newBidPrice);
            lot.makeBid(newBidPrice, this);
        } catch (BidAlreadyMadeException e) {
            log.info("{}: Bid was already made for {}", this, lot);
        } catch (LotAlreadySoldException e) {
            log.info("{}: {} has already been sold", this, lot);
        } catch (BidTooLowException e) {
            log.info("{}: bid {} was too low for a {}", this, newBidPrice, lot);
        } catch (BidException e) {
            log.warn("{}: Failed to make a bit for a lot {}", this, lot, e);
        }
    }

}
