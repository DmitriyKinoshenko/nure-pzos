package ua.nure.pzos.lab2.auction;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@AllArgsConstructor
public class Auction {

    public static final int MAX_NEW_BID_WAIT_MSEC = Bidder.MAX_THINKING_TIME_MSEC;
    private final List<Lot> lots;
    private final List<Bidder> bidders;

    public void startAuction() {
        bidders.forEach(Bidder::enterAuction);

        for(Lot lot : lots) {
            log.info("Considering new lot {}", lot);

            bidders.forEach(bidder -> bidder.newLot(lot));

            Bidder lotWinBidder = null;
            while (true) {
                Bidder nextLotWinBidder = lot.waitUntilNewBid(MAX_NEW_BID_WAIT_MSEC);
                if (nextLotWinBidder == null) {
                    lot.sold();
                    break;
                }

                lotWinBidder = nextLotWinBidder;

                bidders.forEach(bidder -> bidder.lotPriceChanged(lot));
            }

            if (lotWinBidder != null) {
                log.info("Lot {} is sold", lot);
                for (Bidder bidder : bidders) {
                    bidder.lotIsSold(lot, lotWinBidder);
                }
            } else {
                log.info("Lot {} is not sold to anyone", lot);
            }

        }
    }

    public void finishAuction() {
        bidders.forEach(Bidder::leave);

        for (Bidder bidder : bidders) {
            try {
                bidder.join();
            } catch (InterruptedException e) {
                log.warn("interruption on waiting bidder {}", bidder, e);
            }
        }
    }

    public static void main(String[] args) {
        log.info("Start the auction");

        List<Bidder> bidders = List.of(new Bidder("Mary", 1000),
                new Bidder("John", 700));

        var lots = List.of(new Lot("MacBook", 500), new Lot("Dell", 300), new Lot("HP", 200));

        Auction auction = new Auction(lots, bidders);
        auction.startAuction();

        auction.finishAuction();

        log.info("Finish the auction");
    }

}
