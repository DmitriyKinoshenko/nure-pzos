package ua.nure.pzos.lab2.auction;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.ToString;
import ua.nure.pzos.lab2.auction.exception.BidAlreadyMadeException;
import ua.nure.pzos.lab2.auction.exception.BidException;
import ua.nure.pzos.lab2.auction.exception.BidTooLowException;
import ua.nure.pzos.lab2.auction.exception.LotAlreadySoldException;

@ToString(exclude = {"currentBidder", "sold"}, includeFieldNames = false)
public class Lot {
    @Getter
    private final String name;

    @Getter
    private final double initialPrice;

    @Getter
    private double currentPrice;

    @Getter
    private Bidder winningBidder;

    private Bidder currentBidder;
    private boolean sold;

    public Lot(String name, double initialPrice) {
        this.name = name;
        this.initialPrice = initialPrice;
        this.currentPrice = initialPrice;
    }

    public void makeBid(double newPrice, Bidder bidder) throws BidException {
        synchronized(this) {
            if (sold) {
                throw new LotAlreadySoldException();
            }

            if (this.currentBidder != null) {
                throw new BidAlreadyMadeException();
            }

            if (newPrice <= currentPrice) {
                throw new BidTooLowException();
            }

            currentPrice = newPrice;
            this.currentBidder = bidder;

            notify();
        }
    }

    @SneakyThrows
    public synchronized Bidder waitUntilNewBid(int maxNewBidWaitMsec) {
        if (currentBidder == null) {
            wait(maxNewBidWaitMsec);
        }

        if (currentBidder != null) {
            winningBidder = currentBidder;
            currentBidder = null;

            return winningBidder;
        }

        return null;
    }

    public synchronized void sold() {
        sold = true;
    }
}
