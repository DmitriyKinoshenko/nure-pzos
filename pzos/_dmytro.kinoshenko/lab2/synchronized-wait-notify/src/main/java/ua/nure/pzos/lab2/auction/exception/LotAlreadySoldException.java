package ua.nure.pzos.lab2.auction.exception;

public class LotAlreadySoldException extends BidException {

    public LotAlreadySoldException() {
        super("Lot is already sold");
    }

}
