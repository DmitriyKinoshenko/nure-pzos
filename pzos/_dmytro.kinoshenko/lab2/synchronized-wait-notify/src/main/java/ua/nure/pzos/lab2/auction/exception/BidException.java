package ua.nure.pzos.lab2.auction.exception;

public abstract class BidException extends Exception {

    public BidException(String message) {
        super(message);
    }

}
