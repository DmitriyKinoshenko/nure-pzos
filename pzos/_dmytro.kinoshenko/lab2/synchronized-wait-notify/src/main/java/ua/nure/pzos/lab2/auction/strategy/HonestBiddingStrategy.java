package ua.nure.pzos.lab2.auction.strategy;

import ua.nure.pzos.lab2.auction.Lot;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

public class HonestBiddingStrategy implements BiddingStrategy {
    public static final int QUOTER_BIDDING_FACTOR = 4;

    private final int bidFactor;

    public HonestBiddingStrategy(int bidFactor) {
        this.bidFactor = bidFactor;
    }

    public HonestBiddingStrategy() {
        this(QUOTER_BIDDING_FACTOR);
    }

    @Override
    public Optional<Double> makeBid(double balance, Lot lot) {
        double lotPrice = lot.getCurrentPrice();

        if (lotPrice >= balance) {
            return Optional.empty();
        }

        if (toMakeABid()) {
            double bidIncrement = (ThreadLocalRandom.current().nextDouble()) * (balance / bidFactor);
            double newBidPrice = lot.getCurrentPrice() + bidIncrement;

            return Optional.of(newBidPrice);
        }

        return Optional.empty();
    }

    private boolean toMakeABid() {
        return ThreadLocalRandom.current().nextBoolean();
    }

}
