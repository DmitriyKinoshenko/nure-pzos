package ua.nure.pzos.lab2.auction.exception;

public class BidTooLowException extends BidException {

    public BidTooLowException() {
        super("New bid price is not bigger than current one");
    }

}
