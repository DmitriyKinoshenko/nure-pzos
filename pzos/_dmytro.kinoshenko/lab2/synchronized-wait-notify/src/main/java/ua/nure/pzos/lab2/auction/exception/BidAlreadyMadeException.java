package ua.nure.pzos.lab2.auction.exception;

public class BidAlreadyMadeException extends BidException {

    public BidAlreadyMadeException() {
        super("Someone already made a bid");
    }

}
