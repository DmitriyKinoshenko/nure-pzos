package ua.darkminimum.gui;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.Color.BLACK;

enum State {

    NOTSET,
    PEN,
    ERASER
}

public class SwingsGUI extends JFrame implements ActionListener {


    protected static JToolBar toolBar;
    protected static JButton buttonPen, buttonEraser, buttonColor, buttonHelp;
    protected static JLabel labelTool, labelColor, labelColorCode;
    protected static Color color;
    protected static State state = State.NOTSET;
    protected static Canvas canvas;
    protected static Dialog dialog;


    public SwingsGUI() {
        super("Application for drawing rectangles");

        Icon icon_col = new ImageIcon("src/main/resources/images/color.png");
        Icon icon_rect = new ImageIcon("src/main/resources/images/rect.png");
        Icon icon_eraser = new ImageIcon("src/main/resources/images/eraser.png");
        Icon icon_help = new ImageIcon("src/main/resources/images/help.png");

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        labelTool = new JLabel("");
        labelColor = new JLabel("");
        labelColorCode = new JLabel("");

        this.setLayout(new BorderLayout());

        toolBar = new JToolBar("Toolbox");

        JPanel p = new JPanel();
        canvas = new Canvas();
        JPanel p1 = new JPanel();

        p1.add(labelTool);
        p1.add(labelColor);
        p1.add(labelColorCode);

        p1.setBackground(Color.lightGray);

        buttonPen = new JButton("rect", icon_rect);
        buttonEraser = new JButton("color", icon_col);
        buttonColor = new JButton("eraser", icon_eraser);
        buttonHelp = new JButton("about", icon_help);

        buttonPen.addActionListener(this);
        buttonEraser.addActionListener(this);
        buttonColor.addActionListener(this);
        buttonHelp.addActionListener(this);

        p.add(buttonPen);
        p.add(buttonColor);
        p.add(buttonEraser);
        p.add(buttonHelp);

        toolBar.add(p);

        add(toolBar, BorderLayout.NORTH);
        add(p1, BorderLayout.SOUTH);
        add(canvas, BorderLayout.CENTER);

        this.setSize(500, 500);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        SwingsGUI gui = new SwingsGUI();
        gui.setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getActionCommand().equals("OK")) {
            dialog.setVisible(false);
        }
        else if(!e.getActionCommand().equals("color") && !e.getActionCommand().equals("about")) {

            state = (e.getActionCommand().equals("rect")) ? State.PEN : State.ERASER;
            labelTool.setText("Selected tool: " + e.getActionCommand() + ". ");
        }
        else if(e.getActionCommand().equals("color")) {

            try {
                JColorChooser colorChooser = new JColorChooser();
                color = JColorChooser.showDialog(null, "Pick a color", BLACK);

                labelColor.setText("Selected color: " + e.getActionCommand() + ". ");
                labelColorCode.setForeground(color);
                labelColorCode.setText(String.valueOf(color.getRGB()));
            }
            catch (Exception exception) {
                System.out.println("Error occurred in SwingGUI: \n\t- " + exception.getMessage());
            }
        }
        else {
            dialog = new JDialog(this, "About", true);
            dialog.setSize(400, 170);

            // create a label
            JLabel l1 = new JLabel("To use penTool - choose first tool and click&drag.");
            JLabel l2 = new JLabel("To clear the canvas - choose second tool and click on canvas.");
            JLabel l3 = new JLabel("To choose the color for the rectangle click on the palette icon.");

            JPanel panel = new JPanel();
            JPanel panelBottom = new JPanel();
            JButton buttonClose = new JButton("OK");
            buttonClose.addActionListener(this);


            panel.add(l1);
            panel.add(l2);
            panel.add(l3);
            panelBottom.add(buttonClose);

            dialog.add(panel, BorderLayout.CENTER);
            dialog.add(panelBottom, BorderLayout.SOUTH);

            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        }
    }
}
