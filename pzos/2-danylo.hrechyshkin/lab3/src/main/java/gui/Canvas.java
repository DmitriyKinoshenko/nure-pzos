package ua.darkminimum.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Canvas extends JPanel implements MouseListener
{

    private int x, y, x2, y2;

    public Canvas()
    {
        setBackground(Color.WHITE);
        this.addMouseListener(this);

    }

    public void drawPerfectRect(Graphics g, int x, int y, int x2, int y2) {
        int px = Math.min(x,x2);
        int py = Math.min(y,y2);
        int pw=Math.abs(x-x2);
        int ph=Math.abs(y-y2);
        g.drawRect(px, py, pw, ph);
        g.fillRect(px,py,pw,ph);
    }

    public void setStartPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setEndPoint(int x, int y) {
        x2 = (x);
        y2 = (y);
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        if(SwingsGUI.state == State.PEN) {
            g.setColor(SwingsGUI.color);
            drawPerfectRect(g, x, y, x2, y2);
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        setStartPoint(e.getX(), e.getY());

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        setEndPoint(e.getX(), e.getY());
        repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(SwingsGUI.state == State.PEN) {
            this.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        }
        else if(SwingsGUI.state == State.ERASER)  {
            this.setCursor(new Cursor(Cursor.HAND_CURSOR));
        }
        else {
            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}