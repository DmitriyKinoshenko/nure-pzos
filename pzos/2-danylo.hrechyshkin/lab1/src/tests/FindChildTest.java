package ua.darkminimum.tests;

import org.junit.Test;
import ua.darkminimum.strings.FindChild;

import static org.junit.Assert.*;

public class FindChildTest {

    @Test
    public void findHlo() {
        String s1 = "harry";
        String s2 = "robby";

        String result = FindChild.find(s1, s2);

        assertEquals("ry", result);
    }

    @Test
    public void findNothing() {
        String s1 = "xx";
        String s2 = "zz";

        String result = FindChild.find(s1, s2);

        assertEquals("error", result);
    }

}