package ua.darkminimum.tests;

import org.junit.Test;
import ua.darkminimum.point.Point2D;
import ua.darkminimum.point.Point3D;
import ua.darkminimum.straight.Straight2D;
import ua.darkminimum.straight.Straight3D;

import static org.junit.Assert.assertTrue;

public class StraightTest {

    /*
    б) Определить класс Прямая на плоскости (в пространстве), параметры которой задаются с помощью Рациональной Дроби.
    Определить точки пересечения прямой с осями координат.
    Определить координаты пересечения двух прямых.
    Создать массив/список/множество объектов и определить группы параллельных прямых.
    */



    public StraightTest() {
    }


    @Test
    public void createStraights() {

        //Straight in 2D and 3D
        Straight2D straight1 = new Straight2D(new Point2D(3, 2), new Point2D(1, 5));
        Straight3D straight2 = new Straight3D(new Point3D(4, 1, 1), new Point3D(1, 2, 3));


    }

    @Test
    public void intersectionWithXY() {

        //Straight in 2D and 3D
        Straight2D straight1 = new Straight2D(new Point2D(-1, 2), new Point2D(2, 5));

        System.out.println("straight1: \n  " + straight1.getWithOX() + "\n  " + straight1.getWithOY());




    }

    @Test
    public void parallel2D() {
        //Straight in 2D and 3D
        Straight2D straight1 = new Straight2D(new Point2D(-1, 2), new Point2D(0, 5));
        Straight2D straight2 = new Straight2D(new Point2D(-4, -2), new Point2D(-3, 1));

        assertTrue(Straight2D.isParallel(straight1, straight2));


    }

    @Test
    public void intersectionOfLines() {
        Straight2D ab = new Straight2D(new Point2D(3,0), new Point2D(0, 3));
        Straight2D pc = new Straight2D(new Point2D(0.75f, 0), new Point2D(0, 3));

        Point2D intersection = Straight2D.lineIntersection(ab.getA(), ab.getB(), pc.getA(), pc.getB());

        System.out.println(intersection.getX() + " " + intersection.getY());

    }

}