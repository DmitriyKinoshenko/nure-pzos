package ua.darkminimum.tests;

import org.junit.Test;
import ua.darkminimum.rational.RationalFraction;

import static org.junit.Assert.*;

public class RationalFractionTest {

    @Test
    public void createRationalFraction() {

        int m = 2;
        int n = 3;
        RationalFraction frac = new RationalFraction(m, n);
    }

    @Test
    public void checkGCD() {
        RationalFraction f = new RationalFraction();
        assertEquals(1, f.findGCM(13, 12));
    }

    @Test
    public void successfulCompareFractions() {

        RationalFraction frac1 = new RationalFraction(1, 3);
        RationalFraction frac2 = new RationalFraction(1, 3);
        assertTrue(RationalFraction.compareFractions(frac1, frac2));

    }

    @Test
    public void unsuccessfulCompareFractions() {

        RationalFraction frac1 = new RationalFraction(4, 2);
        RationalFraction frac2 = new RationalFraction(1, 2);
        assertFalse(RationalFraction.compareFractions(frac1, frac2));
    }

    @Test
    public void checkEqualFractions() {

        RationalFraction a = new RationalFraction(2, 3);
        RationalFraction b = new RationalFraction(4, 6);
        assertTrue(RationalFraction.compareFractions(a, b));
    }

    @Test(expected = IllegalArgumentException.class)
    public void divideByZeroFraction() {

        int m = 2;
        int n = 0;
        RationalFraction frac = new RationalFraction(m, n);
    }

    @Test
    public void createAndCheckFraction() {

        int n = 2;
        int m = 3;

        RationalFraction frac = new RationalFraction();
        frac.setFraction(m,n);
        assertTrue(frac.getM() == m && frac.getN() == n);

    }

    @Test
    public void testRangeForFractionArray() {

        //testRangeForFractionArray(-10, 10, 1,8);     +
        //testRangeForFractionArray(-10, 10, -10, -1); -
        //testRangeForFractionArray(-10, 10, -10, 10); +

        int minM = -10;
        int maxM = 10;
        int minN = -10;
        int maxN = -1;

        RationalFraction[] frac = new RationalFraction[10];

        for (int i = 0; i < frac.length; i++) {
            RationalFraction elem = RationalFraction.randomFraction(minM, maxM, minN, maxN);


            frac[i] = elem;
            assertTrue((frac[i].getM() >= minM) && (frac[i].getM() <= maxM) && (frac[i].getN() >= minN) && (frac[i].getN() <= maxN));
        }
    }

    @Test
    public void addFractionsWithSameDivider() {

        RationalFraction frac1 = new RationalFraction(4, 2);
        RationalFraction frac2 = new RationalFraction(1, 2);
        RationalFraction resultFrac = new RationalFraction(5,2);
        assertTrue(RationalFraction.compareFractions(resultFrac, RationalFraction.addFractions(frac1, frac2)));
    }

    @Test
    public void subtractFractionsWithDifferentDivider() {

        RationalFraction a = new RationalFraction(3, 5);
        RationalFraction b = new RationalFraction(134, 21);
        RationalFraction result = new RationalFraction(-607, 105);
        assertTrue(RationalFraction.compareFractions(RationalFraction.subtractFractions(a, b), result));
    }

    @Test
    public void multiplyFractions() {
        RationalFraction a = new RationalFraction(7, 15);
        RationalFraction b = new RationalFraction(3, 35);

        RationalFraction result = RationalFraction.multiplyFraction(a, b);
        assertTrue(RationalFraction.compareFractions(result, new RationalFraction(1, 25)));
    }

    @Test
    public void divideFractions() {
        RationalFraction a = new RationalFraction(3, 5);
        RationalFraction b = new RationalFraction(6, 7);

        RationalFraction result = RationalFraction.divideFraction(a, b);
        assertTrue(RationalFraction.compareFractions(result, new RationalFraction(7, 10)));
    }

    @Test
    public void operationUnderFraction() {

        RationalFraction[] array = new RationalFraction[9];
        RationalFraction[] array1;

        for (int i = 0; i < array.length; i++) {
            RationalFraction elem = RationalFraction.randomFraction(-10, 10, -10, 10);

            array[i] = elem;
        }

        array1 = array.clone();
        RationalFraction.operationFraction(array);

        for (int i = 0; i < array.length; i++) {
            if((i % 2 == 0) && ( i+1 < array.length)) {
                array[i] = RationalFraction.subtractFractions(array[i], array[i + 1]);
            }

            assertTrue(RationalFraction.compareFractions(array1[i], array[i]));

        }
    }

}