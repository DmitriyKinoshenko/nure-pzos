package ua.darkminimum.tests;

import org.junit.Test;
import ua.darkminimum.matrix.Matrix;

import static org.junit.Assert.*;

public class MatrixTest {

/*
    Ввести с консоли n-размерность матрицы a [n] [n].
    Задать значения элементов матрицы в интервале значений от -n до n с помощью датчика случайных чисел.
    Построить матрицу, вычитая из элементов каждой строки матрицы ее среднее арифметическое.
 */

    Matrix matrix = new Matrix(4);

    @Test
    public void generateNumberWithBorders() {
        int number = Matrix.createNumberForMatrix(4, -4);
        assertTrue(number >= -4 && number <= 4);
    }

    @Test(expected = NegativeArraySizeException.class)
    public void generateMatrixWithImproperSize() throws NegativeArraySizeException {
        int i = -1;
        matrix = new Matrix(i);

    }

    @Test
    public void generateMatrix() {

        assertEquals(matrix.getRowsCount(),4);
        assertEquals(matrix.getColumnCount(), 4);

        for(int i = 0; i < 4; ++i) {
            for(int j = 0; j < 4; ++j) {
                int currentElement = matrix.getElement(i, j); //matrix[i][j];
                assertTrue(currentElement >= -4 && currentElement <= 4);
            }
        }


    }

    @Test
    public void subtractAverageFromRow() {

        Matrix matrix2 = matrix;
        matrix.subtractAverageFromRow();

        for(int i = 0; i < matrix.getColumnCount(); ++i) {
            int average = 0;

            for(int j = 0; j < matrix.getRowsCount(); ++j) {
               average += matrix2.getElement(i, j);
            }

            average = (int)(average / matrix.getRowsCount());

            for(int j = 0; j < matrix.getRowsCount(); ++j) {
                assertTrue(matrix2.getElement(i, j) == matrix.getElement(i, j) + average);
            }
        }

    }
}