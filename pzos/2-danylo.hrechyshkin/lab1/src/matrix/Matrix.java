package ua.darkminimum.matrix;

public class Matrix {

    private int rowsCount;
    private int columnCount;
    private int[][] matrix;

    public static int createNumberForMatrix(int max, int min) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public Matrix(int i) {

        this.columnCount = i;
        this.rowsCount = i;

        matrix = new int[i][i];

        for (int z = 0; z < i; ++z) {
            for (int j = 0; j < i; ++j) {

                int number = 1;
                matrix[z][j] = createNumberForMatrix(i, -i);
            }
        }

    }

    public int getRowsCount() {
        return rowsCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public int getElement(int i, int j) {
        return matrix[i][j];
    }

    public void subtractAverageFromRow() {
        for(int i = 0; i < this.columnCount; ++i) {
            int average = 0;

            for(int j = 0; j < this.rowsCount; ++j) {
                average += matrix[i][j];
            }

            average = (int)(average / this.rowsCount);

            for(int j = 0; j < this.rowsCount; ++j) {
                matrix[i][j] -= average;
            }
        }
    }
}
