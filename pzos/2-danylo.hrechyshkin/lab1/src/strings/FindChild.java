package ua.darkminimum.strings;

import java.lang.reflect.Array;

public class FindChild {

    public static String find(String s1, String s2) {

        StringBuilder result = new StringBuilder();

        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();

        boolean isEmpty = true;

        for (int i = 0; i < str1.length; i++) {

            for (int j = 0; j < str2.length; j++) {

                if(str1[i] == str2[j]) {

                    isEmpty = false;
                    result.append(str1[i]);
                    ++i;

                }
            }

        }

        if(isEmpty) {
            result.append("error");
        }

        return result.toString();
    }
}
