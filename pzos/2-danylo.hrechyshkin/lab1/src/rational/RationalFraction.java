package ua.darkminimum.rational;

public class RationalFraction {

    private float m = 1;
    private float n = 1;

    public RationalFraction() {
    }

    public RationalFraction(float m, float n) {

        if (n == 0) {
            throw new IllegalArgumentException("The fraction divider can't be set as 0.");
        }

        this.m = m;
        this.n = n;
    }

    public float getM() {
        return m;
    }

    public float getN() {
        return n;
    }

    public int findGCM(float a, float b) {

        a = Math.abs(a);
        b = Math.abs(b);

        while (a != 0 && b != 0) {

            if (a > b)
                a = a % b;
            else
                b = b % a;

        }
        return (int)(a + b);
    }

    public void reduceFraction() {

        float gcm = findGCM(this.m, this.n);

        if (gcm != 1) {
            this.m = this.m / gcm;
            this.n = this.n / gcm;
        }


    }

    public RationalFraction setFraction(float m, float n) {

        if (n == 0) {
            throw new IllegalArgumentException("The fraction divider can't be changed to 0.");
        }
        this.n = n;
        this.m = m;

        return this;
    }

    public static RationalFraction randomFraction(int minM, int maxM, int minN, int maxN)  {

        int m;
        int n = 0;

        m = randomIntegerBetween(maxM, minM);

        if (minN < 0 && maxN > 0) {
            int s = (int) (Math.random() * 2) + 0;

            switch (s) {
                case 0:
                    n = randomIntegerBetween(-1, minN);
                case 1:
                    n = randomIntegerBetween(maxN, 1);
            }
        } else {
            n = randomIntegerBetween(maxN, minN);
        }

        return new RationalFraction(m, n);
    }

    @Override
    public String toString() {
        return m + "/" + n;
    }

    public static int randomIntegerBetween(int max, int min) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    //it has to be normal
    public static boolean compareFractions(RationalFraction a, RationalFraction b) {

        a.reduceFraction();
        b.reduceFraction();

        return a.getM() == b.getM() && a.getN() == b.getN();
    }

    public static RationalFraction addFractions(RationalFraction a, RationalFraction b) {

        RationalFraction result = new RationalFraction();

        if (a.getN() == b.getN())
            result.setFraction(a.getM() + b.getM(), a.getN());
        else
            result.setFraction(a.getM() * b.getN() + b.getM() * a.getN(), a.getN() * b.getN());

        return result;
    }

    public static RationalFraction subtractFractions(RationalFraction a, RationalFraction b) {

        RationalFraction result = new RationalFraction();

        if (a.getN() == b.getN())
            result.setFraction(a.getM() - b.getM(), a.getN());
        else
            result.setFraction(a.getM() * b.getN() - b.getM() * a.getN(), a.getN() * b.getN());

        return result;
    }

    public static void operationFraction(RationalFraction[] frac) {

        for (int i = 0; i < frac.length; i++) {
            frac[i].reduceFraction();
            if (i % 2 == 0 && ( i+1 < frac.length)) {

                frac[i] = RationalFraction.addFractions(frac[i], frac[i + 1]);
            }
        }
    }

    public static RationalFraction multiplyFraction(RationalFraction a, RationalFraction b) {

        RationalFraction result = new RationalFraction(a.getM() * b.getM(), a.getN() * b.getN());
        result.reduceFraction();

        return result;
    }

    public static RationalFraction divideFraction(RationalFraction a, RationalFraction b) {

        RationalFraction result = new RationalFraction(a.getM() * b.getN(), a.getN() * b.getM());
        result.reduceFraction();

        return result;
    }

    public RationalFraction invert() {

        this.m *= -1;
        return this;
    }

}



