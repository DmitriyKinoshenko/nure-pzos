package ua.darkminimum.point;

public class Point2D {

    private float x;
    private float y;

    public Point2D(float x, float y) {
        this.x = x;
        this.y = y;

    }

    @Override
    public String toString() {
        return "x=" + x + "; y=" + y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }


}
