package ua.darkminimum.straight;

import ua.darkminimum.point.Point2D;
import ua.darkminimum.point.Point3D;
import ua.darkminimum.rational.RationalFraction;

public class Straight3D  {

    private Point3D a;
    private Point3D b;

    private Point2D oX;
    private Point2D oY;
    private Point2D oZ;


    public Straight3D(Point3D a, Point3D b) {
        this.a = a;
        this.b = b;
        intersectionWithXYZ();
    }


    /*Check the XY and intersection with X*/
    public Point3D findIntersectionWithX() {

        RationalFraction left = new RationalFraction(a.getX() * -1 * (b.getY() - a.getY()), b.getX() - a.getX());
        RationalFraction right = new RationalFraction(a.getY(), 1);
        RationalFraction result = RationalFraction.addFractions(left, right);

        Point3D point = new Point3D(0, result.getM() / result.getN(), 0);

        return point;
    }

    /*Check the XY and intersection with Y*/
    public Point3D findIntersectionWithY() {

        RationalFraction left = new RationalFraction((b.getX() - a.getX() ) * a.getY() * -1, b.getY() - a.getY());
        RationalFraction right = new RationalFraction(a.getX(), 1);
        RationalFraction result = RationalFraction.addFractions(left, right);

        return new Point3D(result.getM() / result.getN(),0, 0);
    }

    /*Check the XZ and intersection with Z*/
    public Point3D findIntersectionWithZ() {

        RationalFraction left = new RationalFraction(-a.getZ()*(b.getX() - a.getX()), b.getZ() - a.getZ());
        RationalFraction right = new RationalFraction(a.getX(), 1);
        RationalFraction result = RationalFraction.addFractions(left, right);

        return new Point3D(result.getM() / result.getN(),0, 0);
    }

    public void intersectionWithXYZ() {

        this.oX = findIntersectionWithX();
        this.oY = findIntersectionWithY();
        this.oZ = findIntersectionWithZ();
    }

    public Point2D getWithOX() {
        return oX;
    }

    public Point2D getWithOY() {
        return oY;
    }

    public Point2D getWithOZ() {
        return oZ;
    }
}
