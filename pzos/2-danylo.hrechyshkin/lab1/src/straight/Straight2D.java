package ua.darkminimum.straight;

import ua.darkminimum.point.Point2D;
import ua.darkminimum.rational.RationalFraction;

public class Straight2D {

    private Point2D a;
    private Point2D b;

    private Point2D oX;
    private Point2D oY;


    public Straight2D(Point2D a, Point2D b) {

        this.a = a;
        this.b = b;
        intersectionWithXY();
    }

    public static boolean isParallel(Straight2D s1, Straight2D s2) {

        RationalFraction slope1 = new RationalFraction(s1.b.getY() - s1.a.getY(), s1.b.getX() - s1.a.getX());
        RationalFraction slope2 = new RationalFraction(s2.b.getY() - s2.a.getY(), s2.b.getX() - s2.a.getX());
        System.out.println(slope1);
        System.out.println(slope2);
        return RationalFraction.compareFractions(slope1, slope2);
    }

    public static Point2D lineIntersection(Point2D A, Point2D B, Point2D C, Point2D D) {

        // Line AB represented as a1x + b1y = c1
        float a1 = B.getY() - A.getY();
        float b1 = A.getX() - B.getX();
        float c1 = a1*(A.getX()) + b1*(A.getY());

        // Line CD represented as a2x + b2y = c2
        float a2 = D.getY() - C.getY();
        float b2 = C.getX() - D.getX();
        float c2 = a2*(C.getX())+ b2*(C.getY());

        float determinant = a1*b2 - a2*b1;

        if (determinant == 0) {
            // The lines are parallel. This is simplified
            // by returning a pair of FLT_MAX
            return new Point2D(Float.MAX_VALUE, Float.MAX_VALUE);
        }
        else {
            float x = (b2*c1 - b1*c2)/determinant;
            float y = (a1*c2 - a2*c1)/determinant;
            return new Point2D(x, y);
        }
    }

    public Point2D findIntersectionWithX() {

        RationalFraction left = new RationalFraction(a.getX() * -1 * (b.getY() - a.getY()), b.getX() - a.getX());
        RationalFraction right = new RationalFraction(a.getY(), 1);

        RationalFraction result = RationalFraction.addFractions(left, right);

        Point2D point = new Point2D(0, result.getM() / result.getN());

        return point;
    }
    public Point2D findIntersectionWithY() {

        RationalFraction left = new RationalFraction((b.getX() - a.getX() ) * a.getY() * -1, b.getY() - a.getY());
        RationalFraction right = new RationalFraction(a.getX(), 1);
        RationalFraction result = RationalFraction.addFractions(left, right);

        return new Point2D(result.getM() / result.getN(),0);
    }

    public void intersectionWithXY() {

        this.oX = findIntersectionWithX();
        this.oY = findIntersectionWithY();
    }

    public Point2D getWithOY() {
        return oY;
    }
    public Point2D getWithOX() {
        return oX;
    }
    public Point2D getA() {
        return a;
    }
    public Point2D getB() {
        return b;
    }

}
