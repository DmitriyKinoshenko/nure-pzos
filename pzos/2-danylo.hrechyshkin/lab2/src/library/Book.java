package library;

import lombok.*;

@AllArgsConstructor
public class Book {

    @Getter
    private final String title;
    @Getter
    private final boolean isReadingRoomOnly;
    @Getter
    private final int pageCount;

}