package library;

import java.util.ArrayList;
import java.util.List;

import lombok.SneakyThrows;
import org.apache.log4j.Logger;

public class Library extends Thread {

    private static final Logger log = Logger.getLogger(Library.class);
    private final List<Book>  availableBooks;
    private final List<Reader> readers;

    public static final int MAX_READING_TIME = 1_000;
    public static final int MIN_READING_TIME = 100;
    public boolean isReading = true;
    public Library(List<Book> availableBooks, List<Reader> readers, String name) {
        super(name);
        this.availableBooks = availableBooks;
        this.readers = readers;

        log.info("Create books list. Consist of " + availableBooks.size() + " books.");
        log.info("Create readers list. Consist of " + readers.size() + " readers.");
    }
    public void openLibrary() throws InterruptedException {


        for (Reader reader : readers) {
            reader.start();
            log.info(reader.getSurname() + " welcome to library!");
            reader.chooseBooks(availableBooks);
            log.info(reader);
            log.info("");

        }


        while (isReading) {

            for (Reader reader : readers  ) {
                if (reader.getBooksNum() == 0) {

                    reader.leave();
                    isReading = false;
                    //readers.remove(reader);

                } else {
                    reader.readBook(availableBooks);
                    isReading = true;
                }


            }
        }

    }

    @SneakyThrows
    public void run() {
        try {

            log.info("Library is opened with following setup: ");
            log.info("\t- books: " + availableBooks.size() + " and readers: " + readers.size());
            openLibrary();
            log.info("Library is closed.");

        } catch (Exception e) {
            log.error("Error occurred while opening and closing library: \n\t - " + e);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        List<Book> books = new ArrayList<>();

        books.add(new Book("451 degrees fahrenheit", false, 245));
        books.add(new Book("Roadside picnic", false, 321));
        books.add(new Book("Crime and Punishment", false, 123));
        books.add(new Book("Sherlock Holmes", true, 1450));
        books.add(new Book("Ukrainian history", true, 3520));
        books.add(new Book("Kobzar", true, 500));
        books.add(new Book("The City", true, 500));

        List<Reader> readers = List.of(
                new Reader("Hrechyshkin", "Danylo"),
                new Reader("Debre", "Victor"),
                new Reader("Prokop`ev", "Stepan")
        );

        Library library = new Library(books, readers, "Library");
        library.start();
        try{
            library.join();
        }
        catch(InterruptedException e){

            log.error(library.getName() + " has been interrupted");
        }

    }
}