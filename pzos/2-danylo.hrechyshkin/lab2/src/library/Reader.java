package library;

import lombok.Getter;
import org.apache.log4j.Logger;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;


public class Reader extends Thread {

    private static final Logger log = Logger.getLogger(Library.class);

    @Getter
    private final String surname;
    private final String name;

    private final List<Book> gatheredBooks = new ArrayList<>();

    public Reader(String surname, String name) {
        this.name = name;
        this.surname = surname;
        setName("Thread-" + surname);

    }

    public synchronized void chooseBooks(List<Book> books) {

        while(books.size() < 3) {
            try {
                this.wait();
            }
            catch (Exception exception) {
                log.info("Error while choosing Books: " + exception.getMessage());
            }
        }

        int countBooks = 2;
        Iterator<Book> iterator = books.iterator();

        while (iterator.hasNext()) {

            if(countBooks != 0) {
                countBooks--;

                gatheredBooks.add(iterator.next());
                iterator.remove();
            }
            else break;
        }

    }

    public void readBook(List<Book> library) throws InterruptedException {

        Book book = gatheredBooks.iterator().next();
        if(book.isReadingRoomOnly()) {
            int readTime = Library.MIN_READING_TIME + ThreadLocalRandom.current().nextInt(Library.MAX_READING_TIME - Library.MIN_READING_TIME);
            TimeUnit.MILLISECONDS.sleep(readTime);
            log.info(this.name + " has returned following book from reading-hall: " + book.getTitle());
        }
        else {
            int readTime = Library.MIN_READING_TIME + ThreadLocalRandom.current().nextInt(Library.MAX_READING_TIME - Library.MIN_READING_TIME);
            TimeUnit.MILLISECONDS.sleep(readTime * 2);
            log.info(this.name + " has returned following book from home: " + book.getTitle());
        }

        returnBookToLibrary(library, book);


    }

    public synchronized void returnBookToLibrary(List<Book> library, Book book) {
        library.add(book);
        gatheredBooks.remove(book);
        notifyAll();

    }

    public void leave() {
        super.interrupt();
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.name).append(" ").append(this.surname).append(": ");

        for (Book book: gatheredBooks) {
            stringBuilder.append(book.getTitle()).append(", ");

        }

        return stringBuilder.toString();

    }
    public int getBooksNum() {
        return gatheredBooks.size();
    }
}