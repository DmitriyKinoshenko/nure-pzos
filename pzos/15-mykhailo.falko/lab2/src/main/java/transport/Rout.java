package transport;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@AllArgsConstructor
public class Rout {
    List<Bus> busList;
    List<Station> stationList;

    public void startWork() {
        busList.forEach(Bus::takeRout);
        try {
            Thread.sleep(555);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Station s : stationList) {
            log.info("Next stop's number is {}", s.getNumber());
            busList.forEach(bus -> {
                bus.nextStop(s);
            });
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void endWork() {
        busList.forEach(Bus::leaveRout);
        for (Bus bus : busList) {
            try {
                bus.join();
            } catch (InterruptedException e) {
                log.warn("interruption on waiting bus {}", bus, e);
            }
        }
    }

    public static void main(String[] args) {
        log.info("The work starts");

        List<Station> rout = List.of(new Station(1, 2, 0), new Station(2, 7, 0), new Station(3, 7, 0));
        List<Bus> buses = List.of(new Bus(1), new Bus(2), new Bus(3));
        Rout r = new Rout(buses, rout);
        r.startWork();

        r.endWork();
    }
}
