package transport;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Bus extends Thread {
    private final int number;
    private Station currentStation;
    private boolean isWorking = true;

    public void takeRout() {
        start();
    }

    public synchronized void nextStop(Station st){
        currentStation=st;
        notify();
    }

    public synchronized void leaveRout() {
        isWorking = false;
        notify();
    }

    public Bus(int n) {
        this.number = n;
        setName("Bus-" + number);
    }

    @SneakyThrows
    @Override
    public void run() {
        log.info("{} is on the ride", number);
        while (true) {
            synchronized (this) {
                while (currentStation == null && isWorking) {
                    wait();
                }
                if (!isWorking) {
                    break;
                }
                park(currentStation);
                unpark(currentStation);
            }
        }
        log.info("{} leaves the ride", number);
    }


    public void park(Station st) throws InterruptedException {
            if (st.getPresentAmount() < st.getCapacity()) {
                st.setPresentAmount(st.getPresentAmount() + 1);
                currentStation = st;
                log.info("bus number {} parked station {} successfully", this.number, st.getNumber());
            } else {
                log.info("{} station is too crowded", st.getNumber());
            }
            sleep(888);
    }

    public void unpark(Station st) throws InterruptedException {
            st.setPresentAmount(st.getPresentAmount() - 1);
            currentStation = null;
            log.info("{} bus is leaving station {}", this.number, st.getNumber());
    }
}
