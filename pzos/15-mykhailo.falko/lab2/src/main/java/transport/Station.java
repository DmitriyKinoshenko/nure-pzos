package transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@AllArgsConstructor
@Getter
@Setter
public class Station {
    private int number;
    private int capacity;
    private int presentAmount;
}
