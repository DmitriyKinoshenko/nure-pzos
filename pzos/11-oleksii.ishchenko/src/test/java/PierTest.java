import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PierTest {
    Pier testPier = new Pier("Testing");
    Ship testShip = new Ship(150, 12);
    @Test
    public void isBusyPierTest(){
        testPier.useOtherShipOnPier(testShip);
        Assert.assertEquals(testShip, testPier.getCurrentShip());
    }
    @Test
    public void isFreePierTest(){
        Assert.assertEquals(null, testPier.getCurrentShip());
    }
    @Test
    public void freePierByMethodTest(){
        testPier.useOtherShipOnPier(testShip);
        testPier.freePier();
        Assert.assertEquals(null, testPier.getCurrentShip());
    }
    @Test
    public void receivingShipQueueTest(){
        List<Ship> testingShipArray = new ArrayList<>();
        testingShipArray.add(testShip);
        testingShipArray.add(new Ship(150, 15));
        testingShipArray.add(new Ship(130, 21));
        Pier.receiveQueueList(testingShipArray);
        Assert.assertArrayEquals(testingShipArray.toArray(), testPier.getCurrentShipQueue().toArray());
    }
    @Test
    public void hasFreeShipTest(){
        testShip.setStatus(Status.WAIT);
        List<Ship> testingShipArray = new ArrayList<>();
        testingShipArray.add(new Ship(150, 15));
        testingShipArray.add(testShip);
        testingShipArray.add(new Ship(130, 21));
        Pier.receiveQueueList(testingShipArray);
        Assert.assertTrue(testPier.hasFreeShip());
    }
}
