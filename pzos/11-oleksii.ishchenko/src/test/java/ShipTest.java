import org.junit.Assert;
import org.junit.Test;

public class ShipTest {
    Ship testShip = new Ship(150, 1);
    @Test
    public void shipStatusTest(){
        testShip.setStatus(Status.ON_PIER);

        Assert.assertEquals(Status.ON_PIER, testShip.getStatus());
    }

    @Test
    public void shipPurposeTest(){
        testShip.setPurpose(Purpose.LOAD);
        Assert.assertNotEquals(Purpose.UNLOAD, testShip.getPurpose());
    }

    @Test
    public void isShipOverload(){
        Assert.assertFalse(testShip.isOverload());
    }

    @Test
    public void isShipEmpty(){
        Assert.assertEquals(0, testShip.getContainersListSize());
    }
}
