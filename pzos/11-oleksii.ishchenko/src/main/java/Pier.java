import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Pier extends Thread {
    private Ship currentShip;
    private static List<Ship> shipList;

    public Pier(String name){
        this.setName(name);
    }

    public synchronized void useOtherShipOnPier(Ship ship){
        currentShip = ship;
    }

    public Ship getCurrentShip() { return currentShip; }

    public void freePier() {
        currentShip.setStatus(Status.FINISH);
        currentShip = null;
    }

    public static void receiveQueueList(List<Ship> pShipQueue)
    {
        shipList = new ArrayList<>(pShipQueue);
    }

    public List<Ship> getCurrentShipQueue() { return shipList; }

    public synchronized boolean hasFreeShip() {
        for (Ship ship : shipList) {
            if (ship.getStatus() == Status.WAIT) {
                useOtherShipOnPier(ship);
                currentShip.setStatus(Status.ON_PIER);
                return true;
            }
        }
        try {
            sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.err.println("Пирс " + this.getName() + " больше не принимает корабли!");
        this.interrupt();
        return false;
    }

    @Override
    public void run() {
        while (hasFreeShip()) {
            switch (currentShip.getPurpose()) {
                case LOAD:
                    while (!currentShip.isOverload()) {
                        currentShip.loadingContainer(new Random().nextInt(10) + 1);
                        System.out.println("Добавил груз на корабль #" + currentShip.numShip + " с пирса " + this.getName() + "!");
                    }
                    break;
                case UNLOAD:
                    while (currentShip.isOverload()) {
                        currentShip.unloadingContainer();
                        System.out.println("Удалил груз из корабля #" + currentShip.numShip + " с пирса " + this.getName() + "!");
                    }
                    break;
                case UNLOAD_LOAD:
                    while (currentShip.isOverload()) {
                        currentShip.unloadingContainer();
                        System.out.println("Удалил груз из корабля #" + currentShip.numShip + " с пирса " + this.getName() + "!");
                    }
                    while (!currentShip.isOverload()) {
                        currentShip.loadingContainer(new Random().nextInt(10) + 1);
                        System.out.println("Добавил груз на корабль #" + currentShip.numShip + " с пирса " + this.getName() + "!");
                    }
                    break;
            }
            freePier();
        }
    }
}
