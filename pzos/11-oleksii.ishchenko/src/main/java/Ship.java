import java.util.ArrayList;
import java.util.List;

public class Ship {
    public final int carryingCapacity;
    public final int numShip;
    private int currentWeight;
    private List<Integer> containers;
    boolean overload;
    private Purpose purpose;
    private Status status;

    Ship(int carryingCapacity, int numShip) {
        this.carryingCapacity = carryingCapacity;
        this.numShip = numShip;
        currentWeight = 0;
        containers = new ArrayList<Integer>();
        overload = false;
        purpose = Purpose.LOAD;
        status = Status.WAIT;
    }

    public void setStatus(Status status){
        this.status = status;
    }

    public Status getStatus(){
        return this.status;
    }

    public void setPurpose(Purpose purpose){
        this.purpose = purpose;
    }

    public Purpose getPurpose(){
        return purpose;
    }


    public synchronized void loadingContainer(int container) {
        if (currentWeight + container <= carryingCapacity) {
            currentWeight += container;
            containers.add(container);
        } else {
            overload = true;
        }
    }

    public synchronized void unloadingContainer() {
        if (currentWeight > 0) {
            currentWeight -= containers.get(containers.size() - 1);
            containers.remove(containers.size() - 1);
        } else {
            overload = false;
        }
    }

    public int getContainersListSize() {
        return containers.size();
    }

    public boolean isOverload() {
        return overload;
    }
}
