import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;



public class FunctionGraphDrawer extends Application {
    final int FUNCGRAPHAMOUNT = 3;
    private boolean isGraphDrawn[] = new boolean[FUNCGRAPHAMOUNT];

    void drawFunctionGraph(LineChart<Number,Number> pLineChart, FunctionGraph pGraph) {
        XYChart.Series series = new XYChart.Series();

        double board = 2 * Math.PI;

        // series.getData().add(new XYChart.Data(1, 2));

        switch (pGraph) {
            case SINUS:
                if (!isGraphDrawn[FunctionGraph.SINUS.ordinal()]) {
                    pLineChart.setTitle("y = sin(x)");
                    series.setName("Sinus");

                    for (double i = -1 * board; i <= board; i += 0.01) {
                        double coordY = Math.sin(i);
                        series.getData().add(new XYChart.Data(i, coordY));
                    }
                    isGraphDrawn[FunctionGraph.SINUS.ordinal()] = true;
                    pLineChart.getData().add(series);
                }
                break;
            case COSINUS:
                if (!isGraphDrawn[FunctionGraph.COSINUS.ordinal()]) {
                    pLineChart.setTitle("y = cos(x)");
                    series.setName("Cosinus");
                    for(double i = -1 * board; i <= board; i+=0.01)
                    {
                        double coordY = Math.cos(i);
                        series.getData().add(new XYChart.Data(i, coordY));
                    }
                    isGraphDrawn[FunctionGraph.COSINUS.ordinal()] = true;
                    pLineChart.getData().add(series);
                }
                break;

            case PARABOLA:
                if (!isGraphDrawn[FunctionGraph.PARABOLA.ordinal()]) {
                    pLineChart.setTitle("y = x^2");
                    series.setName("Parabola");

                    for (double i = -16; i <= 16; i += 0.01) {
                        double coordY = i * i;
                        series.getData().add(new XYChart.Data(i, coordY));
                    }
                    isGraphDrawn[FunctionGraph.PARABOLA.ordinal()] = true;
                    pLineChart.getData().add(series);
                }
                break;

            default:
                for(int i = 0; i < FUNCGRAPHAMOUNT; i++)
                    isGraphDrawn[i] = false;
                pLineChart.setTitle("");
                pLineChart.getData().clear();
                break;
        }

    }

    @Override public void start(Stage stage) {
        stage.setTitle("Function Graphs (c)Oleksii Ishchenko, 2021 KhNURE");

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();

        final LineChart<Number,Number> lineChart =
                new LineChart<>(xAxis,yAxis);

        lineChart.setTitle("");

        Button btnClear = new Button("Clear chart");
        btnClear.setPrefWidth(80);
        btnClear.setCursor(Cursor.HAND);

        btnClear.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                drawFunctionGraph(lineChart, FunctionGraph.CLEAR);

            }
        });

        Button btnSinus = new Button("Sinus");
        btnSinus.setPrefWidth(80);
        btnSinus.setCursor(Cursor.HAND);
        btnSinus.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                drawFunctionGraph(lineChart, FunctionGraph.SINUS);
            }
        });

        Button btnCosinus = new Button("Cosinus");
        btnCosinus.setPrefWidth(80);
        btnCosinus.setCursor(Cursor.HAND);
        btnCosinus.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                drawFunctionGraph(lineChart, FunctionGraph.COSINUS);
            }
        });

        Button btnParabola = new Button("Parabola");
        btnParabola.setPrefWidth(80);
        btnParabola.setCursor(Cursor.HAND);
        btnParabola.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                drawFunctionGraph(lineChart, FunctionGraph.PARABOLA);
            }
        });

        BorderPane root = new BorderPane(btnClear, lineChart, btnSinus, btnCosinus, btnParabola);
        root.setAlignment(btnClear, Pos.BOTTOM_CENTER);
        root.setAlignment(lineChart, Pos.CENTER);
        root.setAlignment(btnSinus, Pos.BOTTOM_LEFT);
        root.setAlignment(btnCosinus, Pos.BOTTOM_LEFT);
        root.setAlignment(btnParabola, Pos.BOTTOM_LEFT);

        Scene scene  = new Scene(root);

        stage.setScene(scene);

        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
