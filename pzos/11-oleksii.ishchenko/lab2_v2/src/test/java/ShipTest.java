import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShipTest {
    Ship testShip = new Ship(150, 1, "2");

    @Test
    public void shipPurposeTest(){
        testShip.setPurpose(Purpose.LOAD);
        Assert.assertNotEquals(Purpose.UNLOAD, testShip.getPurpose());
    }
    @Test
    public void shipPurposeTest2(){
        testShip.setPurpose(Purpose.UNLOAD_LOAD);
        Assert.assertEquals(Purpose.UNLOAD_LOAD, testShip.getPurpose());
    }

    @Test
    public void isShipOverload(){
        Assert.assertFalse(testShip.isOverload());
    }

    @Test
    public void isShipEmpty(){
        Assert.assertEquals(0, testShip.getContainersListSize());
    }

    @Test
    public void receivingPierQueueTest(){
        List<Pier> testingPierArray = new ArrayList<>();
        Pier testPier = new Pier("Testing"), headPier = new Pier("HeadPier");
        testingPierArray.add(testPier);
        testingPierArray.add(headPier);

        Ship.fillPierList(testingPierArray);
        Assert.assertArrayEquals(testingPierArray.toArray(), Ship.getCurrentPierList().toArray());
    }
}
