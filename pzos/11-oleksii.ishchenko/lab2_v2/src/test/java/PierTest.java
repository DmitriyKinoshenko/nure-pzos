import org.junit.Assert;
import org.junit.Test;

public class PierTest {
    Pier testPier = new Pier("Testing");
    Ship testShip = new Ship(150, 12, "23");
    @Test
    public void isShipGotOut(){
        testPier.freePier();
        Assert.assertEquals(null, testPier.getCurrentShip());
    }
    @Test
    public void isBusyPierTest(){
        testPier.setShip(testShip);
        Assert.assertEquals(testShip, testPier.getCurrentShip());
    }
    @Test
    public void pierStatusTest(){
        testPier.setStatus(Status.FREE);
        Assert.assertEquals(Status.FREE, testPier.getStatus());
    }
}
