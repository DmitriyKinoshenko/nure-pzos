import java.util.ArrayList;
import java.util.List;

public class Ship extends Thread{
    public final int carryingCapacity;
    public final int numShip;
    private int currentWeight;
    private List<Integer> containers;
    boolean overload;
    private Purpose purpose;
    private Pier currentPier;
    private static List<Pier> pierList;

    Ship(int carryingCapacity, int numShip, String pName) {
        this.carryingCapacity = carryingCapacity;
        this.numShip = numShip;
        currentWeight = 0;
        containers = new ArrayList<Integer>();
        overload = false;
        purpose = Purpose.LOAD;
        this.setName(pName);
    }

    public void setPurpose(Purpose purpose){
        this.purpose = purpose;
    }

    public Purpose getPurpose(){
        return purpose;
    }

    public static void fillPierList(List<Pier> pList){
        pierList = pList;
    }
    public static List<Pier> getCurrentPierList()
    {
        return pierList;
    }

    public synchronized void loadingContainer(int container) {
        if (currentWeight + container <= carryingCapacity) {
            currentWeight += container;
            containers.add(container);
        } else {
            overload = true;
        }
    }

    public synchronized void unloadingContainer() {
        if (currentWeight > 0) {
            currentWeight -= containers.get(containers.size() - 1);
            containers.remove(containers.size() - 1);
        } else {
            overload = false;
        }
    }

    public int getContainersListSize() {
        return containers.size();
    }

    public boolean isOverload() {
        return overload;
    }

    public synchronized void takePier(){
        while(true)
            for (int i = 0; i < pierList.size(); i++)
                if (pierList.get(i).getStatus() == Status.FREE) {
                    pierList.get(i).setStatus(Status.BUSY);
                    currentPier = pierList.get(i);
                    currentPier.setShip(this);
                    System.out.println("Корабль " + numShip + " занял пирс " + currentPier.getName());
                    return;
                }
    }

    public synchronized void getOutFromPier(){
        currentPier.freePier();
        currentPier = null;
    }

    @Override
    public void run()
    {
        takePier();
        System.out.println("Корабль " + numShip + " переходит к обслуживанию на " + currentPier.getName());
        currentPier.serveShip();
        System.out.println("Корабль " + numShip + " покидает " + currentPier.getName());
        getOutFromPier();
        this.interrupt();
    }
}
