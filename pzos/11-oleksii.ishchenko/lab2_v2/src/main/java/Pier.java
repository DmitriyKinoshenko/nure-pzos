import java.util.Random;

public class Pier {
    private Ship currentShip;
    private Status pierStatus;
    private String pierName;

    public Pier(String name){
        pierStatus = Status.FREE;
        pierName = name;
    }
    public String getName(){
        return pierName;
    }

    public Status getStatus(){
        return pierStatus;
    }
    public Ship getCurrentShip(){
        return currentShip;
    }

    public void setStatus(Status pStatus){
        pierStatus = pStatus;
    }
    public void setShip(Ship pShip){
        currentShip = pShip;
    }


    public synchronized void serveShip(){
        switch (currentShip.getPurpose()) {
            case LOAD:
                while (!currentShip.isOverload()) {
                    currentShip.loadingContainer(new Random().nextInt(10) + 1);
                    System.out.println("Добавил груз на корабль #" + currentShip.numShip + " с пирса " + getName() + "!");
                }
                break;
            case UNLOAD:
                while (currentShip.isOverload()) {
                    currentShip.unloadingContainer();
                    System.out.println("Удалил груз из корабля #" + currentShip.numShip + " с пирса " + getName() + "!");
                }
                break;
            case UNLOAD_LOAD:
                while (currentShip.isOverload()) {
                    currentShip.unloadingContainer();
                    System.out.println("Удалил груз из корабля #" + currentShip.numShip + " с пирса " + getName() + "!");
                }
                while (!currentShip.isOverload()) {
                    currentShip.loadingContainer(new Random().nextInt(10) + 1);
                    System.out.println("Добавил груз на корабль #" + currentShip.numShip + " с пирса " + getName() + "!");
                }
                break;
        }
    }

    public synchronized void freePier() {
        pierStatus = Status.FREE;
        currentShip = null;
    }
}
