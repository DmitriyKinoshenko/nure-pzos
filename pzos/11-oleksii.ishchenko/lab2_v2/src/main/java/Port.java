/*
 * Корабли заходят в порт для разгрузки/загрузки контейнеров.
 * Число контейнеров, находящихся в текущий момент в порту и на корабле,
 * должно быть неотрицательным и непревышающим заданную грузоподъемность судна и вместимость порта.
 * В порту работает несколько причалов.
 * У одного причала может стоять один корабль.
 * Корабль может загружаться у причала, разгружаться или выполнять оба действия.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Port {
    public static void main(String[] args) {
        System.err.println("Порт открылся!");

        List<Pier> piers = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            piers.add(new Pier("Пирс " + i));
        }
        Ship.fillPierList(piers);

        List<Ship> ships = new ArrayList<>();
        for (Integer i = 0; i < 8; i++) {

            Ship ship = new Ship(new Random().nextInt(100) + 50, i, i.toString());

            switch (new Random().nextInt(2)) {
                case 0:
                    while (!ship.isOverload()) {
                        ship.loadingContainer(new Random().nextInt(10) + 1);
                    }
                    ship.setPurpose(Purpose.UNLOAD);
                    break;
                case 1:
                    while (!ship.isOverload()) {
                        ship.loadingContainer(new Random().nextInt(10) + 1);
                    }
                    ship.setPurpose(Purpose.UNLOAD_LOAD);
                    break;
                case 2:
                    break;
            }
            ships.add(ship);
        }

        for (int i = 0; i < 8; i++) {
            ships.get(i).start();
        }
    }
}