import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Controller {
    @FXML
    Pane pane;
    @FXML
    Circle ball1, ball2;
    @FXML
    Rectangle rec1, rec2;

    @FXML
    void initialize() {
        ball1.setOnMouseDragged(event -> drag(event));
        ball2.setOnMouseDragged(event -> drag(event));
        rec1.setOnMouseDragged(event -> drag(event));
        rec2.setOnMouseDragged(event -> drag(event));
        ball1.setOnScroll(event -> scroll(event));
        ball2.setOnScroll(event -> scroll(event));
        rec1.setOnScroll(event -> scroll(event));
        rec2.setOnScroll(event -> scroll(event));

    }

    public void drag(MouseEvent event) {
        Node n = (Node)event.getSource();
        n.setTranslateX(n.getTranslateX() + event.getX());
        n.setTranslateY(n.getTranslateY() + event.getY());
    }

    public void scroll(ScrollEvent event) {
        Node n = (Node)event.getSource();
        n.setScaleX(n.getScaleX() + event.getDeltaY() / 50);
        n.setScaleY(n.getScaleY() + event.getDeltaY() / 50);
    }

    @FXML
    void makeBlue() {
        ball1.setFill(Color.BLUE);
        ball2.setFill(Color.BLUE);
        rec1.setFill(Color.BLUE);
        rec2.setFill(Color.BLUE);
    }
    @FXML
    void makeGreen() {
        ball1.setFill(Color.GREEN);
        ball2.setFill(Color.GREEN);
        rec1.setFill(Color.GREEN);
        rec2.setFill(Color.GREEN);
    }
    @FXML
    void makeWhite() {
        ball1.setFill(Color.WHITE);
        ball2.setFill(Color.WHITE);
        rec1.setFill(Color.WHITE);
        rec2.setFill(Color.WHITE);
    }
    @FXML
    void makeRed() {
        ball1.setFill(Color.RED);
        ball2.setFill(Color.RED);
        rec1.setFill(Color.RED);
        rec2.setFill(Color.RED);
    }
}
