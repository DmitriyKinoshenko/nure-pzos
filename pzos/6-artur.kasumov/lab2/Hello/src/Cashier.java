import java.util.Deque;


class Cashier extends Thread {
    private volatile Deque<Customer> list;
    private String name;

    Cashier(String name, Deque<Customer> list) {
        this.name = name;
        this.list = list;
        this.setDaemon(true);
        start();
    }

    @Override
    public void run() {
        while (true) {
            Customer customer = null;
            synchronized (list) {
                while (list.size() == 0) {
                    try {
                        list.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                customer = list.poll();
                list.notifyAll();
            }
            System.out.println(this + " have start to serve " + customer);
            System.out.println(customer + " was served by " + this);
        }
    }

    @Override
    public String toString() {
        return "Cashier{" +
                "name='" + name + '\'' +
                '}';
    }
}