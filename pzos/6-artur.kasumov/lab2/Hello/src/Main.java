import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Deque<Customer> customers = new LinkedList<>();
        List<Cashier> cashiers = new LinkedList<>();

        for (int i = 0; i < 3; i++) {
            cashiers.add(new Cashier("Cashier " + i, customers));
        }

        for (int i = 0; i < 10; i++) {
            synchronized (customers) {
                customers.add(new Customer("Customer " + i));
                customers.notifyAll();
            }
        }

        Thread.sleep(3000);
        System.out.println("All customers have been served");

    }
}


