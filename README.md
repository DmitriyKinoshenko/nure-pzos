#PZOS

- Labs link: https://docs.google.com/document/d/17AXEGooCOA8Srs8RQoi7KLEePxRuHJOoCgG-zNm5W1w/edit

- DL course link: https://dl.nure.ua/course/view.php?id=7411

##New user quick guide
1. Register on [Bitbucket] with NURE email.
2. Notify your teacher that you've registered.
3. Later on you will receive invitation email to join this project.
4. Create a Pull Request (PR) with your lab. It's worth to create one PR per lab.
5. Labs are placed at `pzos/{your-variant-number}-{your-name}.{your-surname}/lab{N}`. Example: `pzos/9-vasya.vetrov/lab1`.

[Bitbucket]: https://bitbucket.org/
